package errs

import (
	"fmt"
	th "gitlab.com/stu-b-doo/thelper"
	"strconv"
	te "testing"
)

func ExampleContext() {

	// function f didn't return any context
	if e := f(); e != nil {
		fmt.Println("Don't know where the error happened:\nError:", e)
	}

	// function g tells us where the error occurred
	if e := g(); e != nil {
		fmt.Println("With context:\nError:", e)
	}

	// Output: Don't know where the error happened:
	// Error: Internal error
	// With context:
	// Error: iteration 2 - Internal error

}

// f calls returnsError but doesn't tell us on which iteration
func f() error {
	for i := range []int{0, 1, 2, 3} {

		if e := returnsError(i); e != nil {
			return e
		}

	}
	return nil
}

// g calls returnsError and provides context as to which iteration the error occurred
func g() error {
	for i := range []int{0, 1, 2, 3} {

		if e := returnsError(i); e != nil {
			return C(e, "iteration", strconv.Itoa(i), "-")
		}

	}
	return nil
}

// returnsError returns an error if i == 2
func returnsError(i int) (err error) {

	if i == 2 {
		err = New("Internal error")
	}
	return
}

func TestC_isnil(t *te.T) {
	// C(nil, ...) should return nil
	var e error
	if n := C(e, "messages"); n != nil {
		t.Log("n should be nil but it's not:", n)
		t.FailNow()
	}

	var e2 error
	if e2 = C(e2, "context"); e2 != nil {
		t.Log("e should be nil but it's not:", e2)
		t.FailNow()
	}
}

func TestC_msg(t *te.T) {

	// test cases
	e := New("error")
	a := "a"
	b := "b:"

	exp := &ContextError{
		err: e,
		msg: a,
	}

	th.Eq(t, exp, C(e, a), "C returns err plus context")
	th.Eq(t, "a error", C(e, a).Error(), "Error called on context message formats correctly")
	th.Eq(t, "a b: error", C(e, a, b).Error(), "Error called on context message formats correctly")
}

func TestCf(t *te.T) {

	th.Eq(t, "msg 1: error", Cf(New("error"), "msg %v:", 1).Error(), "Error called on context message formats correctly")
}

func TestFatalIf(t *te.T) {
	var e error

	// shouldn't fail
	FatalIf(e, "e is nil so this call should NOT fail:")

	// should fail
	e = New("non-nil error")
	FatalIf(e, "e is not nil so this call SHOULD fail:")
}
