helper funcs for errors in golang

See [GoDoc](https://godoc.org/gitlab.com/stu-b-doo/errs)

```
// Context returns an error that provides an additional context message before the error
func errs.Context(err error, contextmsg string) error
```

# Quick start

```
	import e "gitlab.com/stu-b-doo/errs"

	// do something that may or may not error
	err := funcThatMightError()
	// tell the stack where the error came from and return if it's not nil
	if n := e.C(err, "funcThatMightError actually errored"); n != nil {
		return
	}

```

# TODO
- do we need a specific contexterror type?

# See also

https://www.weinertworks.com/2018/11/27/better-golang-errors-with-errors-wrap.html

Subsequently found errors.Wrap is doing the same thing: 
https://github.com/pkg/errors
