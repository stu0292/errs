// package errs helper funcs for errors
package errs

import (
	"fmt"
	"errors"
	"log"
)

// C (Context) returns a new error that prefixes the error with a message
// (message elements separated by space).
// Returns nil if e == nil
func C(e error, msg ...string) error {

	if e == nil {
		return nil
	}

	var message string
	for _, s := range msg {
		message = message + " " + s
	}

	return &ContextError{e, message[1:]}
}

// Cf behaves like C but instead of msg elements, a format string and elements are passed to fmt.Sprintf
func Cf(e error, format string, a ...interface{}) error {
	return C(e, fmt.Sprintf(format, a...))
}

// Context (deprecated) wrapper for C
func Context(e error, msg ...string) error {
	return C(e, msg...)
}

// ContextError is an Error type that allows a context message to be added to an error
type ContextError struct {
	err error
	msg string
}

func (e ContextError) Error() string {
	return e.msg + " " + e.err.Error()
}

// New is a convenience wrapper to errors.New
func New(text string) error {
  return errors.New(text)
}

// FatalIf calls log.Fatal(msg, err) if err != nil. 
// Generally for use at the top level main package.
func FatalIf(err error, msg string) {
	if err != nil {
		log.Fatal(msg, err)
	}
}
